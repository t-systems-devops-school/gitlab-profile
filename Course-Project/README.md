# Course project 🚀

<img width="989" alt="image" src="https://user-images.githubusercontent.com/10992037/183438285-24f3d9c5-9952-4648-a40d-ed34f421368c.png">

## Task

The main goal is: Create CI/CD pipeline for depolying the app to k8s.

Detailed description:

1. Dockerize frontend and backend of [course project](https://gitlab.com/t-systems-devops-school/course-project)
2. Implement simple smoke tests.
3. [Create gitlab runners in AWS via terraform & ansible](https://gitlab.com/t-systems-devops-school/5-IAC#homework)
4. Create CI/CD pipeline
5. Run the app in Kubernetes: Implement deploy to different envs: dev и prod
6. Mandatory requirement: Keeping of sensitive data in repos is strictly prohibited for each student. For it you will be deprived of exam scores. 


## Asessemnt

Individual, based on critical design review.

## Nice to see

* CD with additional tooling(helm charts, kustomize, ytt)
* Versioning
* Monitoring / centralized logging
* Secure communication between components

## Course project diagram

The course project consists of homework of different modules. The red color and bold lines indicate the homework required for the course project.

```plantuml
 file " homework  "
 file " homework as part of course project " #EB937F
```
\
\
[1-DevToolKit](https://gitlab.com/t-systems-devops-school/1-DevToolKit) \
[2-Containers](https://gitlab.com/t-systems-devops-school/2-Containers) \
[3-CICD](https://gitlab.com/t-systems-devops-school/3-CICD) \
[4-Introduction-to-Cloud](https://gitlab.com/t-systems-devops-school/4-Introduction-to-Cloud) \
[5-IAC](https://gitlab.com/t-systems-devops-school/5-IAC) \
[6-Orchestration](https://gitlab.com/t-systems-devops-school/6-Orchestration) \
[7-Observability](https://gitlab.com/t-systems-devops-school/7-Observability)


```plantuml
package "[[https://gitlab.com/t-systems-devops-school/1-DevToolKit 1-DevToolKit]] " #FEFECE{
  file " #homework #devtoolkit1 "
  file " #homework #devtoolkit2 " #EB937F
  " #homework #devtoolkit1 " -down-> " #homework #devtoolkit2 "
}
package "[[https://gitlab.com/t-systems-devops-school/2-Containers 2-Containers]] " #FEFECE{
  file " #homework #containers1 "
  file " #homework #containers2 " #EB937F
  " #homework #containers1 " -down-> " #homework #containers2 "
}
package "[[https://gitlab.com/t-systems-devops-school/3-CICD 3-CICD]] " #FEFECE{
  file " #homework #cicd1 " 
  file " #homework #cicd2_1 " #EB937F
  file " #homework #cicd2_2 " #EB937F
  file " #homework #cicd3_1 " #EB937F
  file " #homework #cicd3_2 " #EB937F
  file " #homework #cicd4_1 "
  file " #homework #cicd4_2 " #EB937F
  " #homework #cicd2_1 " -[#brown,thickness=3]down-> " #homework #cicd2_2 "
  " #homework #cicd2_2 " -[#brown,thickness=3]down-> " #homework #cicd3_1 "
  " #homework #cicd3_1 " -[#brown,thickness=3]down-> " #homework #cicd3_2 "
  " #homework #cicd3_2 " -down-> " #homework #cicd4_1 "
  " #homework #cicd4_1 " -down->" #homework #cicd4_2 "
  " #homework #cicd1 "  -> " #homework #cicd2_1 "
}
package "[[https://gitlab.com/t-systems-devops-school/4-Introduction-to-Cloud 4-Introduction-to-Cloud]] " #FEFECE{
 file " #homework #clouds " 
} 
package "[[https://gitlab.com/t-systems-devops-school/5-IAC 5-IAC]] " #FEFECE {
  file " #homework #iac_1 " 
  file " #homework #iac_2 " #EB937F
  file " #homework #iac_3 " #EB937F
  " #homework #iac_1 " -[#brown,thickness=3]down-> " #homework #iac_2 "
  " #homework #iac_2 " -[#brown,thickness=3]down-> " #homework #iac_3 "
}
package "[[https://gitlab.com/t-systems-devops-school/6-Orchestration 6-Orchestration]] " #FEFECE{
 file " #homework #orchestration1 "
 file " #homework #orchestration2 " #EB937F
 file " #homework #orchestration3_1 " #EB937F
 file " #homework #orchestration3_2 " #EB937F
 " #homework #orchestration1 " -->" #homework #orchestration2 "
 " #homework #orchestration3_1 " -[#brown,thickness=3]down-> " #homework #orchestration3_2 "
 " #homework #orchestration2 " -[#brown,thickness=3]down-> " #homework #orchestration3_1 "
}
package "[[https://gitlab.com/t-systems-devops-school/7-Observability 7-Observability]] " #FEFECE{
 file " #homework #observability " #EB937F
}
" #homework #devtoolkit2 "  -[#brown,thickness=3]down--> " #homework #cicd2_1 "
" #homework #devtoolkit2 "  -[#brown,thickness=3]down--> " #homework #iac_1 "
" #homework #devtoolkit2 " -[#brown,thickness=3]down-> " #homework #containers2 "
" #homework #containers2 " -[#brown,thickness=3]down--> " #homework #cicd3_1 "
" #homework #clouds " -[#brown,dashed,thickness=3]down--> " #homework #iac_1 "
" #homework #cicd3_2 " -[#brown,thickness=3]down->  " #homework #orchestration2 "
" #homework #cicd4_2 "  -[#brown,thickness=3]down--> " #homework #orchestration3_1 "
" #homework #orchestration3_2 " -[#brown,thickness=3]down--> " #homework #observability "
" #homework #cicd1 " -[#brown,thickness=3]down->  " #homework #iac_3 "
```
