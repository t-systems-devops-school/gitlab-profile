# DevOps school content

0. Workspace preparation consultation
    * [Linux](https://gitlab.com/t-systems-devops-school/gitlab-profile/-/blob/main/Workspace-Preparation/Linux/README.md?ref_type=heads)
    * [macOS](https://gitlab.com/t-systems-devops-school/gitlab-profile/-/blob/main/Workspace-Preparation/macOS/README.md?ref_type=heads)
    * [Windows](https://gitlab.com/t-systems-devops-school/gitlab-profile/-/blob/main/Workspace-Preparation/Windows/README.md?ref_type=heads)
1. [DevToolKit: Git, Gradle, Python](https://gitlab.com/t-systems-devops-school/1-DevToolKit)
2. [Containers](https://gitlab.com/t-systems-devops-school/2-Containers)
3. [GitLab CI/CD](https://gitlab.com/t-systems-devops-school/3-CICD)
4. [Introduction to Cloud](https://gitlab.com/t-systems-devops-school/4-Introduction-to-Cloud)
5. [IAC: Ansible, Terraform](https://gitlab.com/t-systems-devops-school/5-IAC)
6. [Orchestration: Kubernetes + Helm](https://gitlab.com/t-systems-devops-school/6-Orchestration)
7. [Observability: Prometheus, Grafana, ELK](https://gitlab.com/t-systems-devops-school/7-Observability)
8. [Course Project](Course-Project/README.md)
